package mx.unam.ciencias;

/*
 * interface Atributos Pokemon, esta interfaz
 */

public interface ComportamientoPokemon{

    /* Metodo toString: este metodo sacara la forma String del pokemon para que
     * se pueda imprimir el pokemon.*/
    @Override public String toString();

    /* Metodo equals: metodo que nos ayuda a comparar el pokemon que mande a llamar
     * el metodo con el que le pasa como parametro.
     */
    @Override public boolean equals(Object o);

    /*
     *
     */
    public LinketList<Pokemon> creaLista();

    /*
     *
     */
    public void agregaPokemon();

    /*
     *
     */
    public void eliminaPokemon();

    /*
     *
     */
    public void imprimePokemon();

    /*
     *
     */
    public int compararPokemon();

}
